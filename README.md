# Soal Shift Modul 4 - D10
Penyelesaian Soal Shift Modul 4 Sistem Operasi 2022\
Kelompok D10
* Halyusa Ard Wahyudi - 5025201088
* Zahra Fayyadiyati - 5025201133
* Nathanael Roviery - 5025201258

## Table of Contents
* [Soal 1](#soal-1)
    * [Soal 1.a](#soal-1a)
    * [Soal 1.b](#soal-1b)
    * [Soal 1.c](#soal-1c)
    * [Soal 1.d](#soal-1d)
    * [Soal 1.e](#soal-1e)
* [Soal 2](#soal-2)
    * [Soal 2.a](#soal-2a)
    * [Soal 2.b](#soal-2b)
    * [Soal 2.c](#soal-2c)
    * [Soal 2.d](#soal-2d)
    * [Soal 2.e](#soal-2e)
* [Soal 3](#soal-3)
    * [Soal 3.a](#soal-3a)
    * [Soal 3.b](#soal-3b)
    * [Soal 3.c](#soal-3c)
    * [Soal 3.d](#soal-3d)
    * [Soal 3.e](#soal-3e)

#  Soal 1
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

Anya adalah adalah seorang programmer wibu yang suka mengoleksi anime. Dia sangat senang membuat program yang dikolaborasikan dengan anime. Suatu hari, Anya sedang ingin membuat system dengan keberapa ketentuan.

## Soal 1.a & 1.b & 1.c & 1.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

**1a**

Semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13
    
    Contoh : 
    “Animeku_/anya_FORGER.txt” → “Animeku_/naln_ULITVI.txt”

**1b**

Semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

**1c**

Apabila direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode.

**1e**

Metode encode pada suatu direktori juga berlaku terhadap direktori yang ada di dalamnya.(rekursif).

**Pembahasan:**

Proses enkripsi file yang berada di dalam direktori dengan awalan "Animeku_" akan dienkripsi apabila fungsi .reddir di panggil pada file tersebut.
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    ...
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) 
    {
        ...

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char *ptAnimeku_ = strstr(path, "Animeku_");
        ...
        if (ptAnimeku_ && !ptIAN_){
            if (de->d_type & DT_DIR){
    	        res = (filler(buf, encodeAtBashNRot13(de->d_name), & st, 0));
            } else {
                encodeFileAnimeku_(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }
        ...
            
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```
Apabila terdapat string Animeku_di dalam path dari file atau directory yang dijalankan .readdir, de->dname, yang merupakan nama dari file tersebut, akan di-encode dengan mem-passing variabel tersebut ke dalam fungsi encodeFileAnimeku_(). Namun, apabila ternyata yang dimaksudkan berupa sebuah directory, maka akan dijalankan fungsi encodeAtBashNRot13(). 

encodeAtBashNRot13() adalah fungsi yang dapat meng-encode suatu array of char menjadi sesuai dengan permintaan soal.
```c
char *encodeAtBashNRot13(char *token)
{
    int len = strlen(token);

    for(int i = 0; i < len; i++)
    {
        if(token[i] >= 65 && token[i] <= 90)
        {
            // huruf besar = atbash
            token[i] = 90 - (token[i] - 65);
        }
        else if(token[i] >= 97 && token[i] <= 122)
        {
            // huruf kecil = rot13
            if(token[i] < 110)
                token[i] += 13;
            else
                token[i] -= 13;
        }
    }
    return token;
}
```
Pada fungsi tersebut, akan dilakukan iterasi pada semua char dari pointer array of char yang diberikan. Apabila char tersebut berupa huruf besar, akan diambil karakter dari belakang dengan mengurangi 90 (ASCII terakhir untuk huruf besar) dengan value char tersebut, ditambah dengan 65 (ASCII pertama untuk huruf besar). Encode akan dilakukan ketika .readdir dijalankan.

Sementara itu, fungsi encodeFileAnimeku_() adalah fungsi yang khusus untuk mengencode suatu file menjadi seperti yang dimaksudkan dalam soal dengan memanggil encodeAtBashNRot13() di dalamnya dan melakukan strcat() dengan ekstensi apabila file itu memiliki detil ekstensi.
```c
char *encodeFileAnimeku_(char *token)
{
    char *ext = NULL;
    ext = strrchr(token, '.'); // mencari occurence terakhir dari '.'

    char fileName[1000];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeAtBashNRot13(fileName));
        strcat(fileName, ext);
    }
    else {
        strcpy(fileName, encodeAtBashNRot13(token));
    }

    strcpy(token, fileName);
    return token;
}
```

Supaya sistem dapat membaca kembali file asli (yang berada di dalam directory /Documents) dari file di dalam mounting yang namanya sudah diencode, dibutuhkab fungsi `find_path()` yang mengembalikan array of char berupa path dari file asli/file sumber yang berada di /Documents
```c
// Mencari path sumber
char *find_path(const char *path)
{
    char fpath[2000];
    char unconstPath[500];
    char encodeString[500];
    strcpy(unconstPath, path);

    char *ptAnimeku_ = strstr(path, "Animeku_");
    ...
    if(ptAnimeku_)
    {
        find_path_Animeku_(ptAnimeku_, encodeString);

        encodeFileAnimeku_(encodeString);
        strcpy(unconstPath, path);
        strcat(unconstPath, encodeString);
    }
    ...
    
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
	else
        sprintf(fpath, "%s%s", dirpath, unconstPath);
    
    printf("fpath %s\n", fpath);
    printf("unconstPath %s\n", unconstPath);
    char *fpathPointer= fpath;
    printf("fpathPointer %s\n", fpathPointer);
    return fpathPointer;
}
```
Apabila terdapat string Animeku_di dalam path dari file atau directory yang ada di dalam mounting (yang sudah terencode), akan dipanggil fungsi `find_path_Animeku_()` yang berfungsi untuk mengambil path setelah string "/Animeku_" karena string tersebut ingin di-decode
```c
char *find_path_Animeku_(char *pt1, char *token)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1)))
    {
        char *pt3 = strstr(pt2, "Animeku_");
        if(pt3 == NULL)
        {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }

    strcpy(token, temp);
    printf("token %s temp %s\n", token, temp);
    return token;
}
```
Setelah didapatkan array of char dari path yang ingin di-decode, akan dipanggil fungsi encodeAtBashNRot13(). Hal ini karena apabila kita memberikan array of char yang sudah ter-encode kembali ke fungsi encodeAtBashNRot13(), array of char tersebut akan terdecode.

Selanjutnya, hasil yang didapatkan akan dicopy dan disatukan dengan path sehingga didapatkan suatu array of char yang merupakan path dari file asli/file sumber.

## Soal 1.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

Setiap data yang terencode akan masuk dalam file “Wibu.log” 
Contoh isi: 
RENAME terenkripsi /home/[USER]/Downloads/hai --> /home/[USER]/Downloads/Animeku_hebat 
RENAME terdecode /home/[USER]/Downloads/Animeku_ --> /home/[USER]/Downloads/Coba

**Pembahasan:**

Di dalam fungsi main(), akan dibuka suatu file yang merupakan "Wibu.log"
```c
int  main(int  argc, char *argv[])
{
    ...
    sprintf(logPathSoal1, "/home/%s/Wibu.log", username);
    

    fileLog1 = fopen(logPathSoal1, "w");
    ...
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
Lalu, di dalam fungsi .rename, terdapat suatu prosedur yang berfungsi untuk menuliskan apa yang dimaksudkan pada soal ke dalam Wibu.log.
```c
static int xmp_rename(const char *from, const char *to)
{
    ...
    // Write to fileLog1
    char logText2[2500];
    // Animeku_
    char *ptToPathAnimeku_ = strstr(toPath, "Animeku_");
    char *ptFromPathAnimeku_ = strstr(fromPath, "Animeku_");
    if(ptToPathAnimeku_ && (ptFromPathAnimeku_ == NULL))
    {
        // ter-encode
        sprintf(logText2, "RENAME terenkripsi %s -> %s\n", fromPath, toPath);
        fputs(logText2, fileLog1);
    }
    else if(ptFromPathAnimeku_ && (ptToPathAnimeku_ == NULL))
    {
        // ter-decode
        if(strstr(toPath, "IAN_"))
            sprintf(logText2, "RENAME terenkripsi %s -> %s\n", fromPath, toPath);
        else
            sprintf(logText2, "RENAME terdekripsi %s -> %s\n", fromPath, toPath);
        fputs(logText2, fileLog1);
    }
    ...
}
```

#  Soal 2
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

Saat Anya sedang sibuk mengerjakan programnya, tiba-tiba Innu datang ke rumah Anya untuk mengembalikan barang yang dipinjamnya. Innu adalah programmer jenius sekaligus teman Anya. Ia melihat program yang dibuat oleh Anya dan membantu Anya untuk menambahkan fitur pada programnya dengan beberapa ketentuan

## Soal 2.a & 2.b & 2.c
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

**2a**

Jika suatu direktori dibuat dengan awalan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode dengan algoritma Vigenere Cipher dengan key “INNUGANTENG” (Case-sensitive, Vigenere).

**2b**

Jika suatu direktori di rename dengan “IAN_[nama]”, maka seluruh isi dari direktori tersebut akan terencode seperti pada no. 2a.

**2c**

Apabila nama direktori dihilangkan “IAN_”, maka isi dari direktori tersebut akan terdecode berdasarkan nama aslinya.

**Pembahasan**

Proses ini mirip dengan proses pada soal nomor 1, yang membedakan adalah dekripsi dan enkripsi yang menggunakan VIgenere Cipher. Proses enkripsi file yang berada di dalam direktori dengan awalan "IAN_" akan dienkripsi apabila fungsi .reddir di panggil pada file tersebut.
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    ...
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) 
    {
        ...

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        ...
        char *ptIAN_ = strstr(path, "IAN_");
        ...
        else if(ptIAN_ && !ptAnimeku_)
        {
            if (de->d_type & DT_DIR){
                res = (filler(buf, encodeVigenereCipher(de->d_name), & st, 0));
            } else {
                encodeFileIAN_(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }
        ...
            
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```
Apabila terdapat substring IAN_di dalam path dari file atau directory yang dijalankan .readdir, de->dname, yang merupakan nama dari file tersebut, akan di-encode dengan mem-passing variabel tersebut ke dalam fungsi encodeFileIAN_(). Namun, apabila ternyata yang dimaksudkan berupa sebuah directory, maka akan dijalankan fungsi encodeVigenereCipher(). 

encodeVigenereCipher() adalah fungsi yang dapat meng-encode suatu array of char menjadi sesuai dengan permintaan soal.
``` c
...
char *encodeVigenereCipher(char *plainText)
{
    int i;
    char cipher;
    char cipherText[1000];
    int cipherValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(cipherText, 0, sizeof(cipherText));

    for (i = 0; i<strlen(plainText); i++)
    {
        if (plainText[i] >= 65 && plainText[i] <= 90)
        {
            cipherValue = (((int)plainText[i]-65) + (toupper(key[i%len])-65)) % 26 + 65;

            cipher = (char)cipherValue;
        }
        else if(plainText[i] >= 97 && plainText[i] <= 122)
        {
            cipherValue = (((int)plainText[i]-97) + (tolower(key[i%len])-97)) % 26 + 97;
            cipher = (char)cipherValue;
        }
        else
            cipher = plainText[i];
        strncat(cipherText, &cipher, 1);
    }

    strcpy(plainText, cipherText);
    return plainText;
}
```
Pada fungsi tersebut, akan dilakukan iterasi pada semua char dari pointer array of char yang diberikan. Enkripsi dijalankan sesuai dengan rumus yang ada pada source code diatas, yaitu menambahkannya dengan value dari key yang letak char-nya sama dengan char dari yang ingin di-encode dan menyesuaikan nilainya dengan ASCII. Maka dari itu, huruf besar dan huruf kecil dibedakan prosedurnya karena memiliki nomor ASCII yang berbeda.

Sementara itu, fungsi encodeFileAnimeku_() adalah fungsi yang khusus untuk mengencode suatu file menjadi seperti yang dimaksudkan dalam soal dengan memanggil encodeAtBashNRot13() di dalamnya dan melakukan strcat() dengan ekstensi apabila file itu memiliki detil ekstensi.
```c
char *encodeFileIAN_(char *token)
{
    char *ext = NULL;
    ext = strrchr(token, '.'); // mencari occurence terakhir dari '.'

    char fileName[1000];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    }
    else {
        strcpy(fileName, encodeVigenereCipher(token));
    }

    strcpy(token, fileName);
    return token;
}
```
Apabila terdapat string IAN_di dalam path dari file atau directory yang ada di dalam mounting (yang sudah terencode), akan dipanggil fungsi find_path_IAN_() yang berfungsi untuk mengambil path setelah string "/IAN_" karena string tersebut ingin di-decode
```c
char *find_path_IAN_(char *pt1, char *token)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1)))
    {
        char *pt3 = strstr(pt2, "IAN_");
        if(pt3 == NULL)
        {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }
    strcpy(token, temp);
    return token;
}
```
Setelah didapatkan array of char dari path yang ingin di-decode, akan dipanggil fungsi decodeFileIAN_() yang akan men-decode path.
```c
char *decodeFileIAN_(char *token)
{
    char *ext = NULL;
    ext = strrchr(token, '.'); // mencari occurence terakhir dari '.'

    char fileName[1000];
    char result[1000] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName)))
        {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }
    else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName)))
        {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }

    strcpy(token, result);
    return token;
}
```
Fungsi decodeFileIAN_() hanya berfungsi untuk menggabungkan komponen yang diperlukan dalam file. Yang melakukan decode dari Vigenere Cipher adalah fungsi decodeVigenereCipher().
```c
char *decodeVigenereCipher(char *cipherText)
{
    int i;
    char text;
    char plainText[1000];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++){
        if (cipherText[i] >= 65 && cipherText[i] <= 90)
        {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0)
            {
                textValue += 26;
            }
            textValue += 65;
            
            text = (char)textValue;
        }
        else if (cipherText[i] >= 97 && cipherText[i] <= 122)
        {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0)
            {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        }
        else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }

    strcpy(cipherText, plainText);
    return cipherText;
}
```
Selanjutnya, hasil yang didapatkan akan dicopy dan disatukan dengan path sehingga didapatkan suatu array of char yang merupakan path dari file asli/file sumber.

## Soal 2.d
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi**

Untuk memudahkan dalam memonitor kegiatan yang dilakukan pada filesystem yang telah dibuat, ia membuat log system pada direktori “/home/[user]/hayolongapain_[kelompok].log”. Dimana log ini akan menyimpan daftar perintah system call yang telah dijalankan pada filesystem.

**Pembahasan**

Pembuatan file log dengan FILE dan mengarahkan ke directory /home/[user]/hayolongapain_D10.log

```c
int  main(int  argc, char *argv[])
{
    char *username = getenv("USER");
    char logPathSoal1[1000];
    char logPathSoal2[1000];

    sprintf(dirpath, "/home/%s/Documents", username);
    sprintf(logPathSoal1, "/home/%s/Wibu.log", username);
    sprintf(logPathSoal2, "/home/%s/hayolongapain_D10.log", username);
    

    fileLog1 = fopen(logPathSoal1, "w");
    fileLog2 = fopen(logPathSoal2, "w");
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

## Soal 2.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi**

Karena Innu adalah seorang perfeksionis, ia membagi isi dari log systemnya menjadi 2 level, yaitu level INFO dan WARNING. Untuk log level WARNING, digunakan untuk mencatat syscall rmdir dan unlink. Sisanya, akan dicatat pada level INFO dengan format sebagai berikut : 

**Pembahasan**

Untuk mencatat waktu dan tanggal sesuai format ke dalam log, digunakan library time.h

*Salah satu contoh pencatatan log*
``` c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::READDIR::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    ...

}

#  Soal 3
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

Ishaq adalah seseorang yang terkenal di kalangan anak informatika seluruh indonesia. Ia memiliki teman yang bernama innu dan anya, lalu ishaq bertemu dengan mereka dan melihat program yang mereka berdua kerjakan  sehingga ia tidak mau kalah dengan innu untuk membantu anya dengan menambah fitur yang ada pada programnya dengan beberapa ketentuan.

## Soal 3.a & 3.b & 3.c & 3.d & 3.e
Source Code: [source](https://gitlab.com/roviery/soal-shift-sisop-modul-4-D10-2022/-/blob/master/anya_D10.c)

**Deskripsi:**

**3a**

Jika suatu direktori dibuat dengan awalan “nam_do-saq_”, maka direktori tersebut akan menjadi sebuah direktori spesial.

**3b**

Jika suatu direktori di-rename dengan memberi awalan “nam_do-saq_”,maka direktori tersebut akan menjadi sebuah direktori spesial.

**3c**

Apabila direktori yang terenkripsi di-rename dengan menghapus “nam_do-saq_” pada bagian awal nama folder maka direktori tersebut menjadi direktori normal.

**3d**

Direktori spesial adalah direktori yang mengembalikan enkripsi/encoding pada direktori “Animeku_” maupun “IAN_” namun masing masing aturan mereka tetap berjalan pada direktori di dalamnya (sifat recursive “Animeku_” dan “IAN_” tetap berjalan pada subdirektori).

**3e**

Pada direktori spesial semua nama file (tidak termasuk ekstensi) pada fuse akan berubah menjadi uppercase insensitive dan diberi ekstensi baru berupa nilai desimal dari biner perbedaan namanya.

Contoh : jika pada direktori asli namanya adalah “isHaQ_KEreN.txt” maka pada fuse akan 
menjadi “ISHAQ_KEREN.txt.1670”. 1670 berasal dari biner 11010000110

**Pembahasan:***

Proses enkripsi file yang berada di dalam direktori dengan awalan "nam_do-saq_" akan dienkripsi apabila fungsi .reddir di panggil pada file tersebut.
```c
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    ...
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) 
    {
        ...

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char *ptnamdosaq_ = strstr(path, "nam_do-saq_");
        ...
        else if (ptNamDoSaq_){
            if (de->d_type & DT_DIR){
                res = (filler(buf, encodeSpecialDir(de->d_name), & st, 0));
            } else {
                encodeSpecialFile(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }

        ...
            
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}
```
Apabila terdapat string nam_do-saq_ dalam path dari file atau directory yang dijalankan .readdir, de->dname, yang merupakan nama dari file tersebut, akan di-encode dengan mem-passing variabel tersebut ke dalam fungsi encodeSpecialFile(). Namun, apabila ternyata yang dimaksudkan berupa sebuah directory, maka akan dijalankan fungsi encodeSpecialDir().
```c
char *encodeSpecialFile(char *token)
{
    int specialValue;
    char special;
    char *ext, charBinary[1000], fileName[1000];
    ext = strrchr(token, '.');
    memset(charBinary, 0, sizeof(charBinary));
    memset(fileName, 0, sizeof(fileName));

    for (int i = 0; i < strlen(token); i++)
    {
        if (token[i] == '.') break;
        if (islower(token[i]))
        {
            specialValue = (int)token[i] - 32;
            special = (char) specialValue;
            strcat(charBinary, "1");
        } 
        else
        {
            special = token[i];
            strcat(charBinary, "0");
        }

        strncat(fileName, &special, 1);
    }

    if (ext)
        strcat(fileName, ext);
    
    strcat(fileName, ".");
 
    long int binary;
    binary = strtol(charBinary, NULL, 10);


    int decimal = 0, base = 1, rem;
    while (binary > 0){
        rem = binary % 10;
        decimal = decimal + rem * base;
        binary = binary / 10;
        base = base * 2;
    }
    
    char decimalChar[1000];
    sprintf(decimalChar, "%d", decimal);

    strcat(fileName, decimalChar);
    strcpy(token, fileName);

    return token;
}
```
```c
char *encodeSpecialDir(char *token)
{
    int i, specialValue;
    char special;
    char fileName[1000];
    memset(fileName, 0, sizeof(fileName));
    for (i = 0; i<strlen(token); i++){
        if (token[i] == '.') break;
        if (islower(token[i])){
            specialValue = (int)token[i] - 32;
            special = (char) specialValue;
        }else{
            special = token[i];
        }

        strncat(fileName, &special, 1);
    }
    strcpy(token, fileName);

    return token;
}
```
Kedua fungsi encodeSpecialFile() dan encodeSpecialDir akan menjadikan semua char pada array of char yang dipassing menjadi kapital. Kemudian, khusus untuk fungsi encodeSpecialFile(), akan dihitung nilai desimal dari binary yang didapatkan dari perbedaan namanya. Kemudian, nilai desimal tersebut ditempelkan kepada nama file dengan tambahan titik didepannya.

Supaya sistem dapat membaca kembali file asli (yang berada di dalam directory /Documents) dari file di dalam mounting yang namanya sudah diencode, dibutuhkab fungsi find_path() yang mengembalikan array of char berupa path dari file asli/file sumber yang berada di /Documents
```c
char *find_path(const char *path)
{
    char fpath[2000];
    char unconstPath[500];
    char encodeString[500];
    strcpy(unconstPath, path);

    char *ptnamdosaq_ = strstr(path, "nam_do-saq_");
    ...
    else if(ptnamdosaq_)
    {
        if((ptAnimeku_ && (strtok_r(ptAnimeku_, "/", &ptAnimeku_) == NULL)) || !ptAnimeku_)
        {
            char directoryLocation[1000];
            find_path_namdosaq_(directoryLocation, unconstPath);
            find_path_namdosaq_filedir_(ptnamdosaq_, encodeString);

            if(strlen(encodeString) != 0){
                decodeSpecial(directoryLocation, encodeString);
                printf("directoryLocation %s\n", directoryLocation);
                printf("encodeString %s\n", encodeString);

                strcpy(unconstPath, directoryLocation);
                printf("unconstPath namdosaq %s\n", unconstPath);
            }
        }
    }
    ...
    
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
	else
        sprintf(fpath, "%s%s", dirpath, unconstPath);
    
    printf("fpath %s\n", fpath);
    printf("unconstPath %s\n", unconstPath);
    char *fpathPointer= fpath;
    printf("fpathPointer %s\n", fpathPointer);
    return fpathPointer;
}
```
Apabila terdapat string nam_do-saq_ di dalam path dari file atau directory yang ada di dalam mounting (yang sudah terencode), akan dipanggil fungsi find_path_namdosaq_() yang berfungsi untuk mengambil path sebelum string "/nam_do-saq_" karena path tersebut akan digunakan untuk membuka direktori dari file asli.
```c
char *find_path_namdosaq_(char *token, char *path)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(path, "/", &path)))
    {
        char *pt3 = strstr(pt2, "nam_do-saq_");
        strcat(temp, "/");
        strcat(temp, pt2);

        if(pt3 != NULL)
            break;
    }
    strcpy(token, temp);
    return token;
}
```
Selanjutnya, akan dipanggil fungsi find_path_namdosaq_() yang berfungsi untuk mengambil path setelah string "/nam_do-saq_" karena string tersebut ingin di-decode.
```c

char *find_path_namdosaq_filedir_(char *pt1, char *token)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1)))
    {
        char *pt3 = strstr(pt2, "nam_do-saq_");
        if(pt3 == NULL)
        {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }
    strcpy(token, temp);
    return token;
}
```
Setelah didapatkan array of char dari path yang ingin di-decode, akan dipanggil fungsi decodeSpecial(). Fungsi tersebut akan melakukan decode dengan cara membaca semua file yang berada di direktori yang sama dengan file yang ingin didekripsi. Dalam iterasi pembacaan tersebut, nama file/direktori kemudian akan dilakukan enkripsi sesuai dengan tipe dari yang dibaca. Apabila bertipe direktori, akan dienkripsi dengan fungsi encodeSpecialDir(). Apabila bertipe file, akan dienkripsi dengan fungsi encodeSpecialFile(). SEtelah itu, hasil dari enkripsi yang didapatkan akan dibandingkan dengan nama file atau direktori yang ingin dicari path asli/sumbernya. Apabila hasil dari pemanggilan strcmp() adalah 0, fungsi akan mengembalikan path sebelum dienkripsi yang disimpan di dalam filenameMemo.
```c
char *decodeSpecial(char *path, char *encodeString)
{
    char realPath[1000];
    strcpy(realPath, dirpath);
    strcat(realPath, path);
    DIR *dp;
    dp = opendir(realPath);
    char filenameDecode[1000], filenameMemo[1000];

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) 
    {
        strcpy(filenameMemo, de->d_name);
        strcpy(filenameDecode, "/");
        strcat(filenameDecode, de->d_name);

        if (de->d_type & DT_DIR)
            encodeSpecialDir(filenameDecode);
        else
            encodeSpecialFile(filenameDecode);

        printf("de->dname %s\n", de->d_name);
        printf("filenameDecode %s\n", filenameDecode);
        printf("encodeString %s\n", encodeString);

        if(strcmp(filenameDecode, encodeString) == 0)
            break;
    }

    strcat(path, "/");
    strcat(path, filenameMemo);
    return path;
}
```
Selanjutnya, hasil yang didapatkan akan dicopy dan disatukan dengan path sehingga didapatkan suatu array of char yang merupakan path dari file/direktori asli/sumber.

