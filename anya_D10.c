#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <ctype.h>
#include <sys/time.h>
#include <time.h>

// Compile
// gcc -Wall `pkg-config fuse --cflags` tes2.c -o tes2 `pkg-config fuse --libs`

FILE *fileLog1;
FILE *fileLog2;
char dirpath[1000];

// Fungsi-fungsi untuk enkripsi soal 1
char *encodeAtBashNRot13(char *token)
{
    int len = strlen(token);

    for(int i = 0; i < len; i++)
    {
        if(token[i] >= 65 && token[i] <= 90)
        {
            // huruf besar = atbash
            token[i] = 90 - (token[i] - 65);
        }
        else if(token[i] >= 97 && token[i] <= 122)
        {
            // huruf kecil = rot13
            if(token[i] < 110)
                token[i] += 13;
            else
                token[i] -= 13;
        }
    }
    return token;
}

char *encodeFileAnimeku_(char *token)
{
    char *ext = NULL;
    ext = strrchr(token, '.'); // mencari occurence terakhir dari '.'

    char fileName[1000];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeAtBashNRot13(fileName));
        strcat(fileName, ext);
    }
    else {
        strcpy(fileName, encodeAtBashNRot13(token));
    }

    strcpy(token, fileName);
    return token;
}

char *find_path_Animeku_(char *pt1, char *token)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1)))
    {
        char *pt3 = strstr(pt2, "Animeku_");
        if(pt3 == NULL)
        {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }

    strcpy(token, temp);
    printf("token %s temp %s\n", token, temp);
    return token;
}

// Fungsi-fungsi untuk enkripsi soal 2
char *encodeVigenereCipher(char *plainText)
{
    int i;
    char cipher;
    char cipherText[1000];
    int cipherValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(cipherText, 0, sizeof(cipherText));

    for (i = 0; i<strlen(plainText); i++)
    {
        if (plainText[i] >= 65 && plainText[i] <= 90)
        {
            cipherValue = (((int)plainText[i]-65) + (toupper(key[i%len])-65)) % 26 + 65;

            cipher = (char)cipherValue;
        }
        else if(plainText[i] >= 97 && plainText[i] <= 122)
        {
            cipherValue = (((int)plainText[i]-97) + (tolower(key[i%len])-97)) % 26 + 97;
            cipher = (char)cipherValue;
        }
        else
            cipher = plainText[i];
        strncat(cipherText, &cipher, 1);
    }

    strcpy(plainText, cipherText);
    return plainText;
}

char *decodeVigenereCipher(char *cipherText)
{
    int i;
    char text;
    char plainText[1000];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++){
        if (cipherText[i] >= 65 && cipherText[i] <= 90)
        {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0)
            {
                textValue += 26;
            }
            textValue += 65;
            
            text = (char)textValue;
        }
        else if (cipherText[i] >= 97 && cipherText[i] <= 122)
        {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0)
            {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        }
        else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }

    strcpy(cipherText, plainText);
    return cipherText;
}

char *encodeFileIAN_(char *token)
{
    char *ext = NULL;
    ext = strrchr(token, '.'); // mencari occurence terakhir dari '.'

    char fileName[1000];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    }
    else {
        strcpy(fileName, encodeVigenereCipher(token));
    }

    strcpy(token, fileName);
    return token;
}

char *decodeFileIAN_(char *token)
{
    char *ext = NULL;
    ext = strrchr(token, '.'); // mencari occurence terakhir dari '.'

    char fileName[1000];
    char result[1000] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName)))
        {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }
    else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName)))
        {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }

    strcpy(token, result);
    return token;
}

char *find_path_IAN_(char *pt1, char *token)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1)))
    {
        char *pt3 = strstr(pt2, "IAN_");
        if(pt3 == NULL)
        {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }
    strcpy(token, temp);
    return token;
}

// Fungsi-fungsi untuk enkripsi soal 3
char *encodeSpecialFile(char *token)
{
    int specialValue;
    char special;
    char *ext, charBinary[1000], fileName[1000];
    ext = strrchr(token, '.');
    memset(charBinary, 0, sizeof(charBinary));
    memset(fileName, 0, sizeof(fileName));

    for (int i = 0; i < strlen(token); i++)
    {
        if (token[i] == '.') break;
        if (islower(token[i]))
        {
            specialValue = (int)token[i] - 32;
            special = (char) specialValue;
            strcat(charBinary, "1");
        } 
        else
        {
            special = token[i];
            strcat(charBinary, "0");
        }

        strncat(fileName, &special, 1);
    }

    if (ext)
        strcat(fileName, ext);
    
    strcat(fileName, ".");
 
    long int binary;
    binary = strtol(charBinary, NULL, 10);


    int decimal = 0, base = 1, rem;
    while (binary > 0){
        rem = binary % 10;
        decimal = decimal + rem * base;
        binary = binary / 10;
        base = base * 2;
    }
    
    char decimalChar[1000];
    sprintf(decimalChar, "%d", decimal);

    strcat(fileName, decimalChar);
    strcpy(token, fileName);

    return token;
}

char *encodeSpecialDir(char *token)
{
    int i, specialValue;
    char special;
    char fileName[1000];
    memset(fileName, 0, sizeof(fileName));
    for (i = 0; i<strlen(token); i++){
        if (token[i] == '.') break;
        if (islower(token[i])){
            specialValue = (int)token[i] - 32;
            special = (char) specialValue;
        }else{
            special = token[i];
        }

        strncat(fileName, &special, 1);
    }
    strcpy(token, fileName);

    return token;
}

char *decodeSpecial(char *path, char *encodeString)
{
    char realPath[1000];
    strcpy(realPath, dirpath);
    strcat(realPath, path);
    DIR *dp;
    dp = opendir(realPath);
    char filenameDecode[1000], filenameMemo[1000];

    struct dirent *de;
    while ((de = readdir(dp)) != NULL) 
    {
        strcpy(filenameMemo, de->d_name);
        strcpy(filenameDecode, "/");
        strcat(filenameDecode, de->d_name);

        if (de->d_type & DT_DIR)
            encodeSpecialDir(filenameDecode);
        else
            encodeSpecialFile(filenameDecode);

        printf("de->dname %s\n", de->d_name);
        printf("filenameDecode %s\n", filenameDecode);
        printf("encodeString %s\n", encodeString);

        if(strcmp(filenameDecode, encodeString) == 0)
            break;
    }

    strcat(path, "/");
    strcat(path, filenameMemo);
    return path;
}

char *find_path_namdosaq_(char *token, char *path)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(path, "/", &path)))
    {
        char *pt3 = strstr(pt2, "nam_do-saq_");
        strcat(temp, "/");
        strcat(temp, pt2);

        if(pt3 != NULL)
            break;
    }
    strcpy(token, temp);
    return token;
}

char *find_path_namdosaq_filedir_(char *pt1, char *token)
{
    char temp[500] = "";
    char *pt2 = NULL;
    while((pt2 = strtok_r(pt1, "/", &pt1)))
    {
        char *pt3 = strstr(pt2, "nam_do-saq_");
        if(pt3 == NULL)
        {
            strcat(temp, "/");
            strcat(temp, pt2);
        }
    }
    strcpy(token, temp);
    return token;
}

// Mencari path sumber
char *find_path(const char *path)
{
    char fpath[2000];
    char unconstPath[500];
    char encodeString[500];
    strcpy(unconstPath, path);

    char *ptAnimeku_ = strstr(path, "Animeku_");
    char *ptIAN_ = strstr(path, "IAN_");
    char *ptnamdosaq_ = strstr(path, "nam_do-saq_");
    if(ptAnimeku_)
    {
        find_path_Animeku_(ptAnimeku_, encodeString);

        encodeFileAnimeku_(encodeString);
        strcpy(unconstPath, path);
        strcat(unconstPath, encodeString);
    }
    else if(ptIAN_)
    {
        printf("aman yoh\n");
        find_path_IAN_(ptIAN_, encodeString);

        decodeFileIAN_(encodeString);
        strcpy(unconstPath, path);
        strcat(unconstPath, encodeString);
    }
    else if(ptnamdosaq_)
    {
        if((ptAnimeku_ && (strtok_r(ptAnimeku_, "/", &ptAnimeku_) == NULL)) || !ptAnimeku_)
        {
            char directoryLocation[1000];
            find_path_namdosaq_(directoryLocation, unconstPath);
            find_path_namdosaq_filedir_(ptnamdosaq_, encodeString);

            if(strlen(encodeString) != 0){
                decodeSpecial(directoryLocation, encodeString);
                printf("directoryLocation %s\n", directoryLocation);
                printf("encodeString %s\n", encodeString);

                strcpy(unconstPath, directoryLocation);
                printf("unconstPath namdosaq %s\n", unconstPath);
            }
        }
    }
    
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
	else
        sprintf(fpath, "%s%s", dirpath, unconstPath);
    
    printf("fpath %s\n", fpath);
    printf("unconstPath %s\n", unconstPath);
    char *fpathPointer= fpath;
    printf("fpathPointer %s\n", fpathPointer);
    return fpathPointer;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::READDIR::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    char fpath[1000];
    strcpy(fpath, find_path(path));

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) 
    {
        if(strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0) 
            continue;

        struct stat st;
        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        char *ptAnimeku_ = strstr(path, "Animeku_");
        char *ptIAN_ = strstr(path, "IAN_");
        char *ptNamDoSaq_ = strstr(path, "nam_do-saq_");
        if (ptAnimeku_ && !ptIAN_){
            if (de->d_type & DT_DIR){
    	        res = (filler(buf, encodeAtBashNRot13(de->d_name), & st, 0));
            } else {
                encodeFileAnimeku_(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }
        else if(ptIAN_ && !ptAnimeku_)
        {
            if (de->d_type & DT_DIR){
                res = (filler(buf, encodeVigenereCipher(de->d_name), & st, 0));
            } else {
                encodeFileIAN_(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }
        else if (ptNamDoSaq_){
            if (de->d_type & DT_DIR){
                res = (filler(buf, encodeSpecialDir(de->d_name), & st, 0));
            } else {
                encodeSpecialFile(de->d_name);
                res = filler(buf, de->d_name, &st, 0);
            }
        }
        else {
            res = filler(buf, de->d_name, &st, 0);
        }
            
        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::GETATTR::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    // getattr
    int res;
    char fpath[1000];
    strcpy(fpath, find_path(path));

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_link(const char *from, const char *to)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::LINK::%s::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, from, to);
    fputs(logText, fileLog2);

    // link
    int res;
    res = link(from, to);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::MKDIR::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    res = mkdir(fpath, mode);
    if (res == -1){
        return -errno;
    }
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev){
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    if (S_ISREG(mode)){
        res = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
        if (res >= 0)
            res = close(res);
    }
    else if (S_ISFIFO(mode))
        res = mkfifo(fpath, mode);
    else
        res = mknod(fpath, mode, rdev);
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_unlink(const char *path)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "WARNING::%02d%02d%d-%02d:%02d:%02d::UNLINK::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    int res;
    res = unlink(path);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "INFO::%02d%02d%d-%02d:%02d:%02d::READ::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    char fpath[1000];
    strcpy(fpath, find_path(path));

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_rename(const char *from, const char *to)
{
    // Write to fileLog2
    // IAN_
    char logText1[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText1, "INFO::%02d%02d%d-%02d:%02d:%02d::RENAME::%s::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, from, to);
    fputs(logText1, fileLog2);

    // rename
    int res;
    char fromPath[1000];
    char toPath[1000];

    sprintf(fromPath, "%s%s", dirpath, from);
    sprintf(toPath, "%s%s", dirpath, to);

    res = rename(fromPath, toPath);

    // Write to fileLog1
    char logText2[2500];
    // Animeku_
    char *ptToPathAnimeku_ = strstr(toPath, "Animeku_");
    char *ptFromPathAnimeku_ = strstr(fromPath, "Animeku_");
    if(ptToPathAnimeku_ && (ptFromPathAnimeku_ == NULL))
    {
        // ter-encode
        sprintf(logText2, "RENAME terenkripsi %s -> %s\n", fromPath, toPath);
        fputs(logText2, fileLog1);
    }
    else if(ptFromPathAnimeku_ && (ptToPathAnimeku_ == NULL))
    {
        // ter-decode
        if(strstr(toPath, "IAN_"))
            sprintf(logText2, "RENAME terenkripsi %s -> %s\n", fromPath, toPath);
        else
            sprintf(logText2, "RENAME terdekripsi %s -> %s\n", fromPath, toPath);
        fputs(logText2, fileLog1);
    }

    printf("rename from %s to %s\n", from, to);
    
    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_rmdir(const char *path)
{
    // Write to fileLog2
    char logText[2500];
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    sprintf(logText, "WARNING::%02d%02d%d-%02d:%02d:%02d::RMDIR::%s\n", tm.tm_mday, tm.tm_mon+1, tm.tm_year+1900, tm.tm_hour, tm.tm_min, tm.tm_sec, path);
    fputs(logText, fileLog2);

    // rmdir
    int res;
    char fpath[2000];

    sprintf(fpath, "%s%s", dirpath, path);
    res = rmdir(path);
    
    if (res == -1)
        return -errno;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .link = xmp_link,
    .mkdir = xmp_mkdir,
    .mknod = xmp_mknod,
    .unlink = xmp_unlink,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
};

int  main(int  argc, char *argv[])
{
    char *username = getenv("USER");
    char logPathSoal1[1000];
    char logPathSoal2[1000];

    sprintf(dirpath, "/home/%s/Documents", username);
    sprintf(logPathSoal1, "/home/%s/Wibu.log", username);
    sprintf(logPathSoal2, "/home/%s/hayolongapain_D10.log", username);
    

    fileLog1 = fopen(logPathSoal1, "w");
    fileLog2 = fopen(logPathSoal2, "w");
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
